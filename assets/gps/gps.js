function geo_success(position)
{
    $('#latitude')[0].innerHTML = "目前緯度:" + position.coords.latitude;
    $('#longitude')[0].innerHTML = "目前經度:" + position.coords.longitude;
}

function getLocation()
{

    const geo_options = {
        enableHighAccuracy: true,
        maximumAge        : 30000,
        timeout           : 27000
    };

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(geo_success, geo_error, geo_options);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function geo_error(error)
{
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }
}
