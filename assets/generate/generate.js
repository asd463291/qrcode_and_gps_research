function generateQRCode()
{
    const title = $('input[name=title]').val();
    const lat = $('input[name=lat]').val();
    const lng = $('input[name=lng]').val();

    const component = title + lat + lng;
    console.log(component);
    const before_component = encodeURIComponent(component);
    console.log(before_component);
    const key = md5(before_component);
    console.log(key);
    
    const qrcode_canvas = $('#QRCodeCanvas');
    const qrcode = qrcode_canvas.qrcode({
        text: key
    });

    console.log(qrcode.find("canvas")[0].toDataURL("image/png"));
}
