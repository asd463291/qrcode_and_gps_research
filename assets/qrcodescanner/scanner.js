$(document).ready(function () {
    $("#open_front_camera").click(function () {
        getUserMedia({video: true})
            .then(gotLocalMediaStream)
            .catch(handleMediaStreamError)
    });

    $("#open_back_camera").click(function () {
        openBackCamera();
    });

    $("#screenshot_button").click(function () {
        screenShot();
        const dataURL = getDataURL();
        insertContent(dataURL);
    });

    openBackCamera();
});

function getUserMedia(constraints)
{
    if ('mediaDevices' in navigator) {
        return navigator.mediaDevices.getUserMedia(constraints);
    }

    if (navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = function (constraints) {
          
            navigator.mediaDevices = {};

            var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

            if (!getUserMedia) {
                return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
            }

            return new Promise(function (resolve, reject) {
                getUserMedia.call(navigator, constraints, resolve, reject);
            });
        }

        return navigator.mediaDevices.getUserMedia(constraints);
    }
}

function openBackCamera()
{
    getUserMedia({video: {facingMode: {exact: "environment"}, focusMode: {ideal: "continuous"}}})
        .then(gotLocalMediaStream)
        .catch(handleMediaStreamError)
}

function gotLocalMediaStream(stream)
{
    const video = document.querySelector('video');

    if ("srcObject" in video) {
        video.srcObject = stream;
    } else {
        video.src = window.URL.createObjectURL(stream);
    }
    video.onloadedmetadata = function (e) {
        video.play();
    };
}

function handleMediaStreamError(error)
{
    console.log('navigator.getUserMedia error: ', error);
    if (error['name'] === 'OverconstrainedError') {
        window.alert('請確認相機功能是否正常!');
    } else {
        window.alert('請打開相機權限!');
    }
}

function screenShot()
{
    const video = document.querySelector('video');
    const canvas = document.querySelector('canvas');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
}

function getDataURL()
{
    const canvas = document.querySelector('canvas');
    return canvas.toDataURL("image/png");
}

function stopMedia()
{
    const video = document.querySelector('video');
    const stream = video.srcObject;
    const tracks = stream.getTracks();
    tracks.forEach(function (track) {
        track.stop();
    });
    video.srcObject = null;
}

function insertContent(dataURL)
{
    $.ajax({
        url: './index.php/API/ContentsController/scanQR',
        method: 'POST',
        dataType: 'JSON',
        data: {
            "image_path": dataURL,
        },
        success: function (data) {
            alert(data['message']);
            location.reload();
        },
        error: function () {
            alert('掃描失敗');
        }
    })
}

function deleteContent(id)
{
    const r = confirm("確定要刪除嗎?");
    if (r === true) {
        $.ajax({
            url: './index.php/API/ContentsController/deleteContent',
            method: 'POST',
            dataType: 'JSON',
            data: {
                "id": id,
            },
            success: function (data) {
                alert(data['message']);
                location.reload();
            },
            error: function () {
                alert('刪除失敗');
            }
        })
    }
}
