$(document).ready(function () {
    initializeMap();

    const map = CreateMap();

    AddGeolocate(map);

    map.on('click', function (e) {
        const element = document.querySelector('[aria-label="Map marker"]');
        if (element !== null) {
            element.remove();
        }
        const to = e.lngLat; //lng, lat
        const greenMarker = new mapboxgl.Marker({
            color: 'green'
        })
            .setLngLat(to) // marker position using variable 'to'
            .addTo(map); //add marker to map
        document.getElementById('click_lat').innerHTML = e.lngLat.lat;
        document.getElementById('click_lng').innerHTML = e.lngLat.lng;
    });
});

function initializeMap()
{
    mapboxgl.accessToken = 'pk.eyJ1IjoiYmxhamphMDkxNiIsImEiOiJja3JiZGVodWk0czN6MnBtdHBmNzhzdGE4In0.9wrMBUQKsqCKXX8_DjJ_Zg';
}

function CreateMap()
{
    const map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox://styles/mapbox/streets-v11', // style URL
        center: [120.5792897, 24.2264924], // starting position [lng, lat]
        zoom: 9 // starting zoom
    });

    return map;
}

function AddGeolocate(map)
{
    const geolocate = new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    });

    map.addControl(geolocate);

    map.on('load', function () {
        geolocate.trigger();
    });

    geolocate.on('geolocate', function () {
        document.getElementById('now_lat').innerHTML = this._accuracyCircleMarker._lngLat.lat;
        document.getElementById('now_lng').innerHTML = this._accuracyCircleMarker._lngLat.lng;
    });
}

function countDistance()
{
    const lat1 = document.getElementById('click_lat').innerHTML;
    const lng1 = document.getElementById('click_lng').innerHTML;
    const lat2 = document.getElementById('now_lat').innerHTML;
    const lng2 = document.getElementById('now_lng').innerHTML;
    const EARTH_RADIUS = 6378.137;
    if ((lat1 === lat2) && (lng1 === lng2)) {
        return 0;
    }
    const radLat1 = getRad(lat1);
    const radLat2 = getRad(lat2);

    const a = radLat1 - radLat2;
    const b = getRad(lng1) - getRad(lng2);

    let s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
    s = s*EARTH_RADIUS;


    document.getElementById('distance').innerHTML = s + 'kilometers';
}

function getRad(d)
{
    return d*Math.PI/180.0;
}

function countDistanceByTurf()
{
    const click_lat = document.getElementById('click_lat').innerHTML;
    const click_lng = document.getElementById('click_lng').innerHTML;
    const now_lat = document.getElementById('now_lat').innerHTML;
    const now_lng = document.getElementById('now_lng').innerHTML;
    const to = [click_lng, click_lat]; //lng, lat
    const from = [now_lng, now_lat];//lng, lat
    const options = {
        units: 'kilometers'
    }; // units can be degrees, radians, miles, or kilometers, just be sure to change the units in the text box to match.

    const distance = turf.distance(to, from, options);

    const value = document.getElementById('distance_mapbox');
    value.innerHTML = "Distance: " + distance + "kilometers";
}
