<?php

header('Access-Control-Allow-Origin: *');

defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mapbox 研究</title>
    <base href="<?php echo base_url();?>">
    <link rel="stylesheet" type="text/css" href="assets/mapbox/mapbox.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="assets/mapbox/mapbox.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
    <script src="https://cdn.jsdelivr.net/npm/@turf/turf@5/turf.min.js"></script>
</head>
<body>

<div>
    <h1>Mapbox</h1>
    <div id='map' style='width: 400px; height: 300px;'></div>
    <div>
        <button id="count_distance" onclick="countDistance()">計算兩點距離by公式</button>
        <button id="count_distance_by_mapbox" onclick="countDistanceByTurf()">計算兩點距離by Turf</button><br>
        公式兩點距離為:<h4 id="distance"></h4><br>
        Turf兩點距離為:<h4 id="distance_mapbox"></h4>
    </div>
    <div id="now">
        現在位置:<br>
        緯度:<h3 id='now_lat'></h3>
        經度:<h3 id='now_lng'></h3>
    </div>
    <div id="click">
        點擊位置:<br>
        緯度:<h3 id='click_lat'>0</h3>
        經度:<h3 id='click_lng'>0</h3>
    </div>
</div>
</body>

</html>

