<?php

header('Access-Control-Allow-Origin: *');

defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GPS 定位</title>
    <base href="<?php echo base_url();?>">
    <link rel="stylesheet" type="text/css" href="assets/gps/gps.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="assets/gps/gps.js"></script>
</head>
<body>

<div>
    <h1>Geolocation</h1>
    <div id="container">
        <button onclick="getLocation()">取得目前位置</button>
    </div>
    <div id="container">
        <div id="latitude">目前緯度:</div>
        <div id="longitude">目前經度:</div>
    </div>
</div>
</body>

</html>

