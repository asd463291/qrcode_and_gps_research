<?php

header('Access-Control-Allow-Origin: *');

defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>QRCodeScanner</title>
    <base href="<?php echo base_url();?>">
    <link rel="stylesheet" type="text/css" href="assets/qrcodescanner/scanner.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="assets/qrcodescanner/scanner.js"></script>
</head>
<body>

<div>
    <h1>MediaDevices + php-qrcode-detector-decoder</h1>
    <div id="container">
        <button id="open_front_camera">開啟鏡頭</button>
        <button id="open_back_camera">開啟後置鏡頭</button>
    </div>
    <div id="container">
        <video id="preview"></video>
    </div>

    <div id="container">
        <canvas></canvas>
        <img src="">
    </div>

    <div id="container">
        <button id="screenshot_button">Screenshot Button</button>
        <button onclick="stopMedia()">Stop</button>
    </div>

    <div id="container">
        <table id="content">
            <thead>
            <tr>
                <?php if ($contents !== 'false') {?>
                    <th colspan="2"> 已經讀取的網頁</th>
                    <th scope="col">刪除</th>
                <?php } else { ?>
                    <th colspan="2"> 尚未讀取任何網頁</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($contents !== 'false') {
                foreach ($contents as $content) {
                    ?>
                    <tr>
                        <td colspan="2"><?php echo $content['content']?></td>
                        <td><button onclick="deleteContent(<?= $content['id']?>)" name="delete">刪除</button></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
</body>

</html>
