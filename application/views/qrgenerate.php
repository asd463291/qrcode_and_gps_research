<?php

header('Access-Control-Allow-Origin: *');

defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>QRCodeGenerater</title>
    <base href="<?php echo base_url();?>">
    <link rel="stylesheet" type="text/css" href="assets/generate/generate.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.18.0/js/md5.min.js"></script>
    <script type="text/javascript" src="assets/generate/generate.js"></script>
</head>
<body>

<div>
    <h1>jquery-qrcode</h1>
    <div id="container">
        <label for="title">
            標題文字:
            <input id="title" name="title" value="靜宜大學" required placeholder="請輸入標題文字">
        </label>
        <label for="lat">
            緯度:
            <input id="lat" name="lat" value="24.2258027" required placeholder="請輸入緯度">
        </label>
        <label for="lng">
            經度:
            <input id="lng" name="lng" value="120.5771913" required placeholder="請輸入經度">
        </label>
        <button id="generate" onclick="generateQRCode()">產生 QRCode</button>
    </div>
    <div id="container">
        <div id="QRCodeCanvas"></div>
    </div>
</div>
</body>

</html>
