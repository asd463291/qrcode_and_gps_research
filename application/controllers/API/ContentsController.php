<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '../vendor/autoload.php';

use Zxing\QrReader;

class ContentsController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ContentsModel');
    }

    /**
     * 新增掃描資料
     *
     * @return void
     */
    public function addContent()
    {
        /** @var String $website A text for the website. */
        $website = $this->input->post('website');
        $result = $this->ContentsModel->add($website);

        if ($result != "false") {
            $array = array(
                'success' => true,
                'message' => "新增成功",
            );
        } else {
            $array = array(
                'success' => false,
                'message' => "新增失敗"
            );
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 刪除掃描資料
     *
     * @return void
     */
    public function deleteContent()
    {
        /** @var int $id ID for the website. */
        $id = $this->input->post('id');
        $result = $this->ContentsModel->delete($id);

        if ($result != "false") {
            $array = array(
                'success' => true,
                'message' => "刪除成功"
            );
        } else {
            $array = array(
                'success' => false,
                'message' => "刪除失敗"
            );
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 掃描照片的 QRcode 並存入資料庫
     *
     * @return void
     */
    public function scanQR()
    {
        $path = $this->input->post('image_path');
        $qrcode = new QrReader($path);
        $text = $qrcode->text();
        if ($text == '0') {
            return;
        }

        $result = $this->ContentsModel->add($text);

        if ($result != "false") {
            $array = array(
                'success' => true,
                'message' => "新增成功",
            );
        } else {
            $array = array(
                'success' => false,
                'message' => "新增失敗"
            );
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}
