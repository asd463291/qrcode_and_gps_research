<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ContentsModel');
    }

    /**
     * 讀取 QRCode 讀取器的頁面
     *
     * @return void
     */
    public function index()
    {
        /**
         * @var array $data Loaded websites
         */
        $data['contents'] = $this->ContentsModel->getAllContent();
        $this->load->view('qrcode_scanner.php', $data);
    }

    /**
     * 讀取 GPS 定位的頁面
     *
     * @return void
     */
    public function gps()
    {
        $this->load->view('gps.php');
    }

    /**
     * 讀取 GPS 定位的頁面
     *
     * @return void
     */
    public function mapbox()
    {
        $this->load->view('mapbox.php');
    }

    /**
     * 讀取 GPS 定位的頁面
     *
     * @return void
     */
    public function qrgenerate()
    {
        $this->load->view('qrgenerate.php');
    }
}
