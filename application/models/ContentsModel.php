<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ContentsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 取得所有掃描資料
     *
     * @return Array | String
     */
    public function getAllContent()
    {
        $this->db->select('id, content, created_at')
            ->from('content');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return "false";
        }
    }

    /**
     * 取得所有掃描資料
     *
     * @param string $website A text for the website.
     *
     * @return String
     */
    public function add($website)
    {
        $data = array(
            'content' => $website,
        );

        $query = $this->db->insert('content', $data);
        if ($this->db->affected_rows() > 0) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * 取得所有掃描資料
     *
     * @param int $id ID for the website.
     *
     * @return String
     */
    public function delete($id)
    {
        $data = array(
            'id' => $id,
        );

        $query = $this->db->delete('content', $data);
        if ($this->db->affected_rows() > 0) {
            return "true";
        } else {
            return "false";
        }
    }
}
